<form name="form" method="post">
	<h3 class="title">API Details</h3>
	<p>Enter API details as provided by Rhino Racks:</p>

	<table class="form-table">
		<tbody>
			<tr>
				<th><label for="username"> Username</label></th>
				<td> <input name="username" id="username" type="text" value="<?php echo $username; ?>" class="regular-text code"></td>
			</tr>
			<tr>
				<th><label for="password">Password</label></th>
				<td> <input name="password" id="password" type="password" value="<?php echo $password; ?>" class="regular-text code"></td>
			</tr>
		</tbody>
	</table>
	<p>To run the synchronization of all roof racks, use the command line interface and run :
		<code>wp racks-sync-by-date run --all --continue</code>
	</p>

	<p>
		The above command syncs all the roof racks. The --continue switch with make the console app look for a stored datetime value in the config/racks-sync-datetime.txt and starts the sync from there. If no file is present, its created, and a start time of '50 years ago' is used.
	</p>


	<p>To run the synchronization of roof racks from a datetime, use the command line interface and run :
		<code>wp racks-sync-by-date run --since '10 hours ago'</code>
	</p>
	<p>
		The format of the `--since` option should be valid <a href="http://php.net/manual/en/datetime.formats.date.php">date format</a>. If no `--since` option is provided, the date is defaulted to `1 day ago`
	</p>

	<hr>

	<p>To run the synchronization of all accessories, use the command line interface and run :
		<code>wp accessories-sync-by-date run --all --continue</code>
	</p>

	<p>This is similar to the roof racks sync with --all options</p>

	<p>
		Run a command similar to the one below synchronize rhino accessories by datetime. Adjust the `--since` option value accordingly. <br>

<code>
wp accessories-sync-by-date run --since="10 hours ago"
</code>
</p>

<p>
** Note: The string provided in the `--since` option should be a valid <a href="http://php.net/manual/en/datetime.formats.date.php">date format</a>.

	</p>


	<h3 class="title">Cart URL</h3>
	<p>You can specify a different URL for your cart:</p>

	<table class="form-table">
		<tbody>
			<tr>
				<th><label for="rhino_cart_url"> Cart</label></th>
				<td> <input name="rhino_cart_url" id="rhino_cart_url" type="text" value="<?php echo $rhino_cart_url; ?>" class="regular-text code"></td>
			</tr>

		</tbody>
	</table><a href="
	"></a>
	<p class="submit">
		<input type="hidden" name="save-options" value="true"/>
		<input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes">
	</p>
</form>
