<?php
/**
 * RhinoRacks Synchronization Tool
 *
 * This plugin synchronizes Rhino Racks products to your local WP + Woocommerce installation
 *
 * @link              http://mrkdevelopment.com/
 * @copyright  		  M R K Development Pty Ltd.
 * @since             1.0.0
 * @package           Rhino Rack Sync
 * @license 		  http://www.gnu.org/licenses/gpl-2.0.txt
 *
 */

require_once __DIR__.'/API/RhinoHelper.php';
require_once __DIR__.'/API/RhinoRack.php';
require_once __DIR__.'/API/Accessories.php';

require_once __DIR__.'/API/RhinoWCProduct.php';
require_once __DIR__.'/API/RoofRacks.php';
require_once __DIR__.'/API/Vehicles.php';

/**
 * Rhino racks sync class for CLI
 */
class RhinoRackSyncCli extends WP_CLI_Command
{

    private $vehicles;
    private $roofRacks;

    private $mainCategory;
    private $makeCategory;
    private $modelCategory;
    private $yearCategory;
    private $bodyCategory;

    private $make;
    private $model;
    private $year;
    private $body;

    private $helper;

    /**
     * Checks if option have been set up from the WP back end
     * and return the array with options
     * @return array Rhinio racks options
     */
    private function getOptions()
    {
        $username = get_option('rhino_username', null);
        $password = get_option('rhino_password', null);

        if (!($username || $password) ) {
            WP_CLI::line('Please configure username, password and api_id using the WP interface.');

            return;
        }

        return array(
                'username' => $username,
                'password' => $password,
            );
    }

    /**
     * Runs the syncronization of the products between RhinoRacks and WP
     */
    public function run()
    {
        $this->helper = new RhinoHelper;

        $this->mainCategory = $this->helper->addTerm('Vehicles');

        $options         = $this->getOptions();
        $this->vehicles  = new Vehicles($options);
        $this->roofRacks = new RoofRacks($options);

        // Browsing the API :
        $this->vehicles->GetVehicleMakes();
        $makes = $this->vehicles->result();

        foreach ($makes as $make) {
            $this->make         = (string)$make->MakeName;
            $this->makeCategory = $this->helper->addTerm((string)$make->MakeName, $this->mainCategory, $this->make . '-' .  'vehicles');
            $this->getModels($make);
        }
    }

    private function getModels($make)
    {
        WP_CLI::line("------ Getting models for maker %s-------\n", $make->MakeName);

        $makeId = $make->MakeID;
        $this->vehicles->GetVehicleModels($makeId);

        $models = $this->vehicles->result();
        foreach ($models as $model) {
            $this->model         = (string) $model->ModelName;
            $this->modelCategory = $this->helper->addTerm((string) $model->ModelName, $this->makeCategory, $this->model . '-' . $this->make);
            $this->getYears($model);
        }
    }

    private function getYears($model)
    {
        $modelId = $model->ModelID;
        WP_CLI::line(sprintf("** Model %s **\n", $modelId));
        $this->vehicles->GetVehicleModelYears($modelId);
        $years = $this->vehicles->result();

        foreach ($years as $year) {
            $this->year         = (string) $year->Year;
            $this->yearCategory = $this->helper->addTerm((string) $year->Year, $this->modelCategory, $this->year . '-' . $this->model . '-' . $this->make);
            $this->getBodyTypes($modelId, $year);
        }
    }

    private function getBodyTypes($modelId, $year)
    {
        $yearId = $year->Year;
        WP_CLI::line(sprintf("** Year %s **\n", $yearId));

        $this->vehicles->GetVehicleBodyTypes($modelId, $yearId);
        $bodyTypes = $this->vehicles->result();

        foreach ($bodyTypes as $bodyType) {
            $this->body         = (string)$bodyType->Name;
            $this->bodyCategory = $this->helper->addTerm((string)$bodyType->Name, $this->yearCategory, $this->body . '-' . $this->year . '-' . $this->model . '-' . $this->make);
            $this->getVehicles($modelId, $yearId, $bodyType);
        }
    }

    private function dd($o, $pass = false)
    {
        var_dump($o);

        if (!$pass) {
            exit;
        }
    }

    private function getVehicles($modelId, $yearId, $bodyType)
    {
        $bodyTypeId = $bodyType->ID;
        WP_CLI::line(sprintf("** Body Type %s **\n", $bodyTypeId));

        $this->vehicles->GetVehicles($modelId, $yearId, $bodyTypeId);
        $listVehicles = $this->vehicles->result();

        foreach ($listVehicles as $vehicle) {
            $this->getRoofRacks($vehicle);
        }
    }

    private function getRoofRacks($vehicle)
    {
        $vehicleId = $vehicle->VehicleID->__toString();

        WP_CLI::line(sprintf("\tFetching roof racks for vehicle %s, %s\n", $vehicleId, $vehicle->VehicleName));

        $params = array('vehicleId' => $vehicleId, 'status' => '1', 'pageIndex' => 1, 'pageSize' => 20);
        WP_CLI::line(json_encode($params));
        while ($this->roofRacks->GetRoofRacksForVehicle($params)) {
            $listRoofRacks = $this->roofRacks->result();
            // WP_CLI::line("Page Index " . $params['pageIndex']);

            if (!$listRoofRacks->RoofRack) {
                break;
            }
            // WP_CLI::line(count($listRoofRacks));
            // WP_CLI::line(json_encode($listRoofRacks));

            foreach ($listRoofRacks->RoofRack as $roofRack) {
                $product = new RhinoWCProduct($roofRack);
                $postID  = $product->save();
                WP_CLI::line(sprintf("\t\t* %s, price %s\n", $roofRack->Name, $roofRack->Price));

                // $this->dd(array($this->mainCategory, $this->vehicleCategory));
                $response = wp_set_object_terms($postID, $this->getCategories(), 'product_cat', false);
                unset($product);
            }

            $params['pageIndex'] = $params['pageIndex'] + 1;
        }
    }

    private function getCategories()
    {
        return    array(
            $this->mainCategory,
            $this->makeCategory,
            $this->modelCategory,
            $this->yearCategory,
            $this->bodyCategory,
        );
    }
}
