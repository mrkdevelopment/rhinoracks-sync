<?php
/**
 * RhinoRacks Synchronization Tool
 *
 * This plugin synchronizes Rhino Racks products to your local WP + Woocommerce installation
 *
 * @link              http://mrkdevelopment.com/
 * @copyright  		  M R K Development Pty Ltd.
 * @since             1.0.0
 * @package           Rhino Rack Sync
 * @license 		  http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * @wordpress-plugin
 * Plugin Name:       Rhino Rack Sync
 * Description:       This plugin synchronizes Rhino Racks products to your local WP + Woocommerce installation
 * Version:           1.0.0
 * Author:            M R K Development
 * Author URI:        http://mrkdevelopment.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       rhinorack-sync
 */

require_once __DIR__ . '/vendor/autoload.php';

define('RHINO_URL', 'http://api.rhinorack.com/');

/**
 * Class RhinoRacksSync
 * Syncronizes RhinoRacks products with your local WP/WooCommerce installation and display a configuration panel in WP backend
 */
class RhinoRacksSync
{

    /**
     * Class constructor
     * Checks if the app is being ran from wp-cli or from the web based interface.
     */
    public function __construct()
    {
        if (defined('WP_CLI') && WP_CLI) {
            require __DIR__.'/RhinoRackSyncCli.php';
            WP_CLI::add_command( 'racks-sync-all', 'RhinoRackSyncCli');

            require __DIR__.'/RhinoRacksUpdateByDateCli.php';
            WP_CLI::add_command( 'racks-sync-by-date', 'RhinoRacksUpdateByDateCli');

            require __DIR__.'/RhinoAccessoriesUpdateByDateCli.php';
            WP_CLI::add_command( 'accessories-sync-by-date', 'RhinoAccessoriesUpdateByDateCli' );
        } else {
            add_action( 'admin_menu', array(&$this, 'registerRhinoSettingsMenu' ) );
            $this->registerRRParser();
        }
    }

    /**
     * Registers the settings menu for configuring Rhino Racks API
     */
    public function registerRhinoSettingsMenu()
    {
        add_options_page('Rhino Racks Settings', 'Rhino Racks Settings', 'manage_options', 'rhino-settings', array(&$this, 'rhinoSettings'));
    }

    /**
     * Display the menu to change Rhino Settings and save the options
     */
    public function rhinoSettings()
    {
        if (!empty($_POST) && isset($_POST['save-options'])) {
            $username       = $_POST['username'];
            $password       = $_POST['password'];
            $rhino_cart_url = $_POST['rhino_cart_url'];
            update_option('rhino_username', $username);
            update_option('rhino_password', $password);
            update_option('rhino_cart_url', $rhino_cart_url);
        } else {
            $username       = get_option('rhino_username', '');
            $password       = get_option('rhino_password', '');
            $rhino_cart_url = get_option('rhino_cart_url');
            if (!$rhino_cart_url) {
                $rhino_cart_url = get_site_url().'/cart/';
            }
        }
        require __DIR__.'/views/rhino-settings.php';
    }

    public function run_rr_parser()
    {
        $rhino_cart_url = get_option('rhino_cart_url');
        if (!$rhino_cart_url) {
            $rhino_cart_url = get_site_url().'/cart/';
        }
        new RRParser($rhino_cart_url);
    }

    public function create_rr_posttype()
    {
        register_post_type( 'rhino-cart',
            array(
              'labels' => array(
                'name'          => __( 'Rhino-cart' ),
                'singular_name' => __( 'Rhino-cart' ),
              ),
              'public'      => true,
              'has_archive' => true,
            )
          );
    }
    /**
     * Register the hook when one command is bieng placed and the user is sent back to the site
     */
    public function registerRRParser()
    {
        require __DIR__.'/XMLParser/RRParser.php';

        add_action('wp', array(&$this, 'run_rr_parser'), 1);

        add_action( 'init', array(&$this, 'create_rr_posttype'));
    }

    // public function registerWooCommerceHooks()
    // {

    //     // add the filter
    //     add_filter( 'woocommerce_single_product_image_html', array(&$this, 'filter_woocommerce_single_product_image_html'), 10, 2 );

    //     add_filter( 'woocommerce_single_product_image_thumbnail_html', array(&$this, 'filter_woocommerce_single_product_image_thumbnail_html'), 15, 4 );
    // }

                // define the woocommerce_single_product_image_html callback
    // public function filter_woocommerce_single_product_image_html($gallery, $id)
    // {
    //     $url = get_post_meta($id, '_rhino_image');

    //     if (!empty($url)) {
    //         $post_title = get_the_title($id);

    //         return sprintf('<img width="600" height="600" src="%s" class="attachment-shop_single wp-post-image" alt="%s" title="%s">', $url[0], $post_title, $post_title);

    //     } else {
    //         return $gallery;
    //     }
    // }

    // define the woocommerce_single_product_image_thumbnail_html callback
    // public function filter_woocommerce_single_product_image_thumbnail_html($image_link, $attachment_id, $id, $image_class)
    // {
    //     die('here');
    //     $url = get_post_meta($id, '_rhino_image');
    //     var_dump($image_link);
    //     var_dump($url);
    //     if (!empty($url)) {
    //         return $url;
    //     } else {
    //         return $image_link;
    //     }
    // }
}

new RhinoRacksSync();
