# Rhino Racks Sync - Synchronization tool for Rhino Racks
---

RhinoRacksync is a WP plugin used to synchronize products from http://portal.rhinoracks.com to your local WP/WooCommerce installation.

To use it, download the source code here: https://bitbucket.org/mrkdevelopment/rhinoracks-sync/downloads
And install the plugin in your wordpress installation

## Requirements

It is highly recommended to use >= php 5.4.

Product sync rely on [WP CLI](http://wp-cli.org/) which are run on the console. These are long running cron jobs. Shared hosts usually do not provide this functionality. Highly recommended to use a VPS.

Rhino Racks Sync relies on [WooCommerce.](http://www.woothemes.com/woocommerce/)

## Configuration
1) Activate the plugin by browsing to your WP admin / Plugins (wp-admin/plugins.php)
2) When the plugin is activated, browse to settings / Rhino Racks Settings (wp-admin/options-general.php?page=rhino-settings)
3) In API Details, fill the username and password as provided by RhinoRacks.
4) You can change the cart URL if your installation of Woocommerce has a specific set up.

## Updating products while ordering from Rhino Racks
Following the instructions from Rhino Racks, you have set up the portal's URL to come back after checkout to your cart. When the plugin is activated, products in the shopping cart will automatically be created and updated, and your order can be placed on your website. There is nothing else to do.

## Synchronizing all the products ##
If you want to improve your user experience and make sure they can browse the catalog of products on your website, you can use WP-CLI to run a synchronization. To do so, run the following command:

```
wp racks-sync-by-date run --all --continue
```
The above command syncs all the roof racks. The --continue switch with make the console app look for a stored datetime value in the config/racks-sync-datetime.txt and starts the sync from there. If no file is present, its created, and a start time of '50 years ago' is used.

```
wp racks-sync-by-date run --since='10 hours ago'
```

This command can be used to sync updated racks in the recent past. The format of the `--since` option should be valid [Date Format](http://php.net/manual/en/datetime.formats.date.php). If no `--since` option is provided, the date is defaulted to `1 day ago`. 

## Synchronizing Accessories ##
Run a command similar to the one below synchronize all the rhino accessories.

```
wp accessories-sync-by-date run --all --continue
```

Run the following command to synchronize accessories updated in the last one hour (UTC timezone)

```
wp accessories-sync-by-date run --since="1 hour ago"
```

** Note: The string provided in the `--since` option should be a valid [date Format](http://php.net/manual/en/datetime.formats.date.php).
