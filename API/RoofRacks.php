<?php
/**
 * RhinoRacks Synchronization Tool
 *
 * This plugin synchronizes Rhino Racks products to your local WP + Woocommerce installation
 *
 * @link              http://mrkdevelopment.com/
 * @copyright         M R K Development Pty Ltd.
 * @since             1.0.0
 * @package           Rhino Rack Sync
 * @license           http://www.gnu.org/licenses/gpl-2.0.txt
 */

require_once __DIR__ . '/../vendor/autoload.php';

/**
 * API controller for RoofRacks
 */
class RoofRacks extends RhinoRack
{

    /**
     * Defines the WSDL and contruct the object
     */
    public function __construct($args)
    {
        $this->wsdl = 'http://api.rhinorack.com/RoofRackWS.asmx?WSDL';
        parent::__construct($args);
    }

    // Returns a list of all roof racks available for the specified vehicle, for the default culture
    public function GetRoofRacksForVehicle($params)
    {
        return $this->request('GetRoofRacksForVehicle', array('GetRoofRacksForVehicle' => $params));
    }

    // Returns a list of all roof racks available for the specified vehicle
    public function GetRoofRacksForVehicleByCulture()
    {
        //TODO
    }

    //returns all the roofracks for a given date
    public function GetRoofRacksForDate($params)
    {
        return $this->request('GetRoofRacksForDate', array('GetRoofRacksForDate' => $params));
    }
}
