<?php

require_once __DIR__ . '/SoapClientTimeout.php';

/**
 * RhinoRacks Synchronization Tool
 *
 * This plugin synchronizes Rhino Racks products to your local WP + Woocommerce installation
 *
 * @link              http://mrkdevelopment.com/
 * @copyright         M R K Development Pty Ltd.
 * @since             1.0.0
 * @package           Rhino Rack Sync
 * @license           http://www.gnu.org/licenses/gpl-2.0.txt
 */

/**
 * Generic object used to connect to the Saop service and get the products
 * Is extended by Vehicles, RoofRacks and Accessories
 *
 */
class RhinoRack
{
    /**
     * $wsdl SOAP definition of the service (URL)
     * @var String
     */
    protected $wsdl;

    /**
     * $username User name to connect with the API
     * @var String
     */
    private $username = null;

    /**
     * $password Password to connect with the API
     * @var String
     */
    private $password = null;

    /**
     * SOAP client
     *
     * @var SoapClient
     */
    public $client;

    /**
     * Class Constructor
     * Initiate service
     * @param array $args the array of [username, password]
     */
    public function __construct($args)
    {
        if (!isset($args['username']) || !isset($args['password'])) {
            throw new Exception("'username', 'password' and 'api_id' are required parameters.");
        }

        $this->username = $args['username'];
        $this->password = $args['password'];

        $this->client = new SoapClientTimeout($this->wsdl,
                            array(
                                  'compression'        => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP,
                                  'trace'              => 1,
                                  'connection_timeout' => 3000,
                            ));

        $this->client->__setTimeout(3000);
    }

    /**
     * Builds the Header for the Soap Authentication
     */
    private function header()
    {
        $authvalues = new SoapVar(array('ns1:UserName' => $this->username, 'ns1:Password' => $this->password), SOAP_ENC_OBJECT);
        $header     =  new SoapHeader(RHINO_URL, "SOAPHeaderAuth", $authvalues, false);

        $this->client->__setSoapHeaders(array($header));
    }

    /**
     * Sends a request to the service
     * @param  String   $command Command to run, according to WSDL
     * @param  Request  $request The current request
     * @return Response the received response
     */
    protected function request($command, $request)
    {
        $this->command = $command;
        $this->header();
        try {
            $this->response = $this->client->__soapCall($command, $request);

            return $this->response;
        } catch (SoapFault $e) {
            // handle issues returned by the web service
            echo $this->client->__getLastResponse();
            echo 'FAILED:' .$e->getMessage();

            return array();
        } catch (Exception $e) {
            // $this->trace();
            echo $this->client->__getLastResponse();
            echo 'FAILED:' .$e->getMessage();

            return array();
        }
    }

    /**
     * Interpreting the answer to a XML format
     * @return XML the answer for previous query
     */
    public function result()
    {
        $command = $this->command.'Result';

        $xml     = @simplexml_load_string($this->response->$command->any);

        return $xml;
    }

    /**
     * Outputs the current answer for debug
     */
    public function trace()
    {
        var_dump($this->client->__getLastRequest());
    }
}
