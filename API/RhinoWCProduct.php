<?php
/**
 * RhinoRacks Synchronization Tool
 *
 * This plugin synchronizes Rhino Racks products to your local WP + Woocommerce installation
 *
 * @link              http://mrkdevelopment.com/
 * @copyright         M R K Development Pty Ltd.
 * @since             1.0.0
 * @package           Rhino Rack Sync
 * @license           http://www.gnu.org/licenses/gpl-2.0.txt
 */

require_once ( ABSPATH . 'wp-admin/includes/image.php' );
require_once __DIR__.'/RhinoHelper.php';
require_once __DIR__.'/Vehicles.php';

/**
 * Synchronize a given product with Woocommerce
 */
class RhinoWCProduct
{
    /**
     * [$data description]
     * @var [type]
     */
    protected $data;

    /**
     * [$update description]
     * @var boolean
     */
    protected $update = false;

    function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Sync rhino racks/components as woocommerce products
     *
     * @param string $stockCode   rhino product code
     * @param string $name        product name
     * @param string $description description
     * @param float  $price       product price
     * @param string $imageURL    image url
     *
     */
    public function sync($stockCode, $name, $description, $price, $imageURL)
    {
        // WP_CLI::line("------ Syncing $stockCode, $name");
        global $wpdb;

        $postID = $wpdb->get_var( sprintf("SELECT post_id FROM $wpdb->postmeta where meta_key='_sku' and meta_value='%s'", $stockCode ));

        if (!is_null($postID)) {
            // WP_CLI::line("Update found");
            $this->update = true;
        } else {
            $this->update = false;
        }

        if (!$this->update) {
            $postID = wp_insert_post(array(
                    'post_content' => $description,
                    'post_title'   => $name,
                    'post_type'    => 'product',
                    'post_parent'  => '',
                    'post_status'  => "publish",
                 ));

            update_post_meta( $postID, 'total_sales', '0');
            update_post_meta( $postID, '_featured', "no" );
            update_post_meta( $postID, '_purchase_note', "" );
            update_post_meta( $postID, '_weight', "" );
            update_post_meta( $postID, '_length', "" );
            update_post_meta( $postID, '_width', "" );
            update_post_meta( $postID, '_height', "" );

            update_post_meta( $postID, '_sale_price_dates_from', "" );
            update_post_meta( $postID, '_sale_price_dates_to', "" );
            update_post_meta( $postID, '_downloadable', 'no');
            update_post_meta( $postID, '_virtual', 'no');
            update_post_meta( $postID, '_sold_individually', "yes" );
            update_post_meta( $postID, '_manage_stock', "no" );
            update_post_meta( $postID, '_backorders', "no" );
            update_post_meta( $postID, '_stock', "" );
        } else {
            wp_update_post( array(
                'ID'           => $postID,
                'post_content' => $description,
                'post_title'   => $name,
           ));
        }

        // WP_CLI::line("POST ID $postID");

        $attributes = array();

        update_post_meta( $postID, '_rhino_image', str_replace('https', 'http', $imageURL));
        update_post_meta( $postID, '_sku', $stockCode );

        update_post_meta( $postID, '_visibility', 'visible' );
        update_post_meta( $postID, '_stock_status', 'instock');
        update_post_meta( $postID, '_regular_price', $price );
        update_post_meta( $postID, '_sale_price', $price );
        update_post_meta( $postID, '_price', $price );

        $this->syncImage($postID, $imageURL);

        $this->updateRackAttributes($postID);

        return $postID;
    }

    /**
     * Update Rack Specifications
     *
     * @param [type] $postID [description]
     *
     * @return [type] [description]
     */
    public function updateRackAttributes($postID)
    {

        // specifications
        if (isset($this->data->Specifications)) {
            foreach ($this->data->Specifications->Specification as $spec) {
                $product_attributes[(string)$spec->Name] = array(
                //Make sure the 'name' is same as you have the attribute
                'name'         => htmlspecialchars(stripslashes((string) $spec->Name)),
                'value'        => (string) $spec->Value,
                'is_visible'   => 1,
                );

                update_post_meta( $postID, '_product_attributes', $product_attributes);
            }
        }

                // links
        if (isset($this->data->Links)) {
            foreach ($this->data->Links->Link as $spec) {
                $product_attributes[(string)$spec->Name] = array(
                //Make sure the 'name' is same as you have the attribute
                'name'         => htmlspecialchars(stripslashes((string) $spec->Name)),
                'value'        => sprintf('<a href="%s" target="blank">View</a>', (string) $spec->FileUrl, (string) $spec->FileUrl),
                'is_visible'   => 1,
                );

                update_post_meta( $postID, '_product_attributes', $product_attributes);
            }
        }
    }

    /**
     * Save the product
     *
     */
    public function save()
    {
        $description = $this->data->Description->__toString();

        // racks
        if (isset($this->data->FeaturesDescription)) {
            $description = (string) $this->data->ListDescription;
            $description .= '<p>&nbsp;</p><h3>Feature Description</h3>';
            $description .= (string) $this->data->FeaturesDescription;
        }

        if (isset($this->data->Media->MediaItem)) {

          // images
            $hasImage = false;
            $iter     = 1;
            foreach ($this->data->Media->MediaItem as $item) {
                $type = (string) $item->ContentType;

                if ($type == 'Image') {
                    if (!$hasImage) {
                        $description .= '<p>&nbsp;</p><h3>Images</h3><ul>';
                    }

                    $hasImage = true;

                    $description .= sprintf('<li><a href="%s" target="blank">Image %s</a></li>', (string) $item->Url,  $iter );
                    $iter++;
                }
            }

            if ($hasImage) {
                $description .= '</ul>';
            }

          // videos
            $hasVideo = false;
            foreach ($this->data->Media->MediaItem as $item) {
                $type = (string) $item->ContentType;

                if ($type == 'Youtube') {
                    if (!$hasVideo) {
                        $description .= '<p>&nbsp;</p><h3>Videos</h3><ul>';
                    }

                    $hasVideo = true;

                    $description .= sprintf('<li><a href="%s" target="blank">%s</a></li>', (string) $item->Url, (string) $item->Title );
                }
            }

            if ($hasVideo) {
                $description .= '</ul>';
            }
        }

        $postID = $this->sync(
                        $this->data->StockCode->__toString(),
                        $this->data->Name->__toString(),
                        $description,
                        $this->data->Price->__toString(),
                        $this->data->MainImage->__toString()
                    );

        // check for components.
        //
        if (isset($this->data->PackageComponents)) {
            foreach ($this->data->PackageComponents as $component) {
                // WP_CLI::line(sprintf("\n Syncing component %s \n", json_encode($component)));
                $this->sync(
                             $component->Component->StockCode->__toString(),
                             $component->Component->Name->__toString(),
                             $component->Component->Name->__toString(),
                             $component->Component->Price->__toString(),
                             $component->Component->Image->__toString()
                     );
            }
        }

        return $postID;
    }

    /**
     * Sync Product Image.
     *
     * @param integer $postID POST ID
     * @param string  $url    image url
     *
     */
    protected function syncImage($postID, $url)
    {
        $wp_upload_dir = wp_upload_dir();
        $urlPathinfo   = pathinfo($url);

        $localPath = $wp_upload_dir['basedir'] . '/rhinoracks/' . $urlPathinfo['basename'];

        if (!file_exists($localPath)) {
            $this->downloadImage($url, $localPath);
        }

        $this->associateAttachment($localPath, $postID);
    }

    public function associateAttachment($localPath, $postID)
    {
        $wp_upload_dir = wp_upload_dir();

        $filetype = wp_check_filetype( basename( $localPath ), null );

        $attachment = array(
            'guid'           => $wp_upload_dir['url'] . '/' . basename( $localPath ),
            'post_mime_type' => $filetype['type'],
            'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $localPath ) ),
            'post_content'   => '',
            'post_status'    => 'inherit',
        );

        $attach_id = wp_insert_attachment( $attachment, $localPath, $postID );

        $attach_data = wp_generate_attachment_metadata( $attach_id, $localPath );
        wp_update_attachment_metadata( $attach_id, $attach_data );
        add_post_meta($postID, '_thumbnail_id', $attach_id);
    }

    public function getCategories()
    {
        $categories = array();
        foreach ($this->data->Categories as $category) {
            $categories[] = array('main' => (string) $category->Category->MainTitle, 'title' => (string) $category->Category->Title);
        }

        return $categories;
    }

    /**
     * Sync Racks Categories using GetVehiclesByRoofRack
     *
     * @param [type] $postID [description]
     *
     * @return [type] [description]
     */
    public function syncRackCategories($postID)
    {
        $helper = new RhinoHelper;

        $vehicles = new Vehicles($helper->getOptions());

        $vehicles->GetVehiclesByRoofRack((string) $this->data->ID);

        $result = json_decode(json_encode($vehicles->result()));

        if (!isset($result->Vehicle)) {
            WP_CLI::line("Empty Result");

            return;
        }

        // normalize array.
        if (is_array($result->Vehicle)) {
            $items = $result->Vehicle;
        } else {
            $items = array($result->Vehicle);
        }

        foreach ($items as $vehicle) {
            if (!is_string($vehicle->Manufacturer) || !is_string($vehicle->Model)) {
                continue;
            }

            // compute data
            $make                     = (string) $vehicle->Manufacturer;
            $model                    = (string) $vehicle->Model;
            $body                     = (string) $vehicle->BodyType;
            list($startYear, $others) = explode('-', (string) $vehicle->StartDate, 2);
            if (!is_string($vehicle->EndDate)) {
                $years   = array($startYear);
                $endYear = date('Y', time());
            } else {
                list($endYear, $others) = explode('-', (string) $vehicle->EndDate, 2);
                $years                  = array();
            }

            foreach (range($startYear, $endYear) as $year) {
                $years[] = $year;
            }

            // create categories and assign to rack.
            foreach ($years as $year) {
                $year    = (string) $year;
                $mainId  = $helper->addTerm('Vehicles');
                $makeId  = $helper->addTerm($make, $mainId, $make . '-' .  'vehicles');
                $modelId = $helper->addTerm($model, $makeId, $model . '-' . $make);
                $yearId  = $helper->addTerm($year, $modelId, $year . '-' . $model . '-' . $make);
                $bodyId  = $helper->addTerm($body, $yearId, $body . '-' . $year . '-' . $model . '-' . $make);

                wp_set_object_terms($postID, array($mainId, $makeId, $modelId, $yearId, $bodyId), 'product_cat', true);
            }

            // WP_CLI::line("$make, $model, $body, " . json_encode($years));
        }

        unset($result);
    }

    /**
     * Download Image
     *
     * @param string $url       http location
     * @param string $localPath absolute local path
     *
     */
    public function downloadImage($url, $localPath)
    {
        set_time_limit(0);
        $fp = fopen ($localPath, 'w+');//This is the file where we save the    information
        $ch = curl_init(str_replace(" ", "%20", $url));//Here is the file we are downloading, replace spaces with %20
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_FILE, $fp); // write curl response to file
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch); // get curl response
        curl_close($ch);
        fclose($fp);
    }
}
