<?php

use Carbon\Carbon;

class RhinoHelper
{

    /**
     * Add WP Term
     *
     * @param [type]  $name   [description]
     * @param boolean $parent [description]
     */
    public function addTerm($name, $parent = false, $slug = null)
    {
        $params = array();

        if ($parent) {
            $params['parent'] = $parent;
        }

        if (!is_null($slug)) {
            $params['slug'] = sanitize_title($slug);
        }

        $response = wp_insert_term($name, 'product_cat', $params);
        if (is_wp_error($response)) {
            return (integer) $response->get_error_data();
        } else {
            return (integer) $response['term_id'];
        }
    }

    /**
     * Checks if option have been set up from the WP back end
     * and return the array with options
     * @return array Rhinio racks options
     */
    public function getOptions()
    {
        $username = get_option('rhino_username', null);
        $password = get_option('rhino_password', null);

        if (!($username || $password) ) {
            WP_CLI::line('Please configure username, password and api_id using the WP interface.');

            return;
        }

        return array(
                'username' => $username,
                'password' => $password,
            );
    }

    /**
     * Compute OR get --since argument.
     *
     * @param string $fileName Name of the file to store the last update timestamp.
     *
     * @return [type] [description]
     */
    public function computeSinceArgument($fileName, $args)
    {
        if (is_null($args[1]['all']) && is_null($args[1]['since'])) {
            die('Bad Input. Please pass atleast --all or --since option');
        }

        if (isset($args[1]['all'])) {
            if (isset($args[1]['continue'])) {
                return $this->getDateTime($fileName);
            }

            $this->setDateTime($fileName, Carbon::createFromTimestamp(strtotime('50 years ago'))->format('Y-m-d\TH:i:s'));

            return $this->getDateTime($fileName);
        }

        if (isset($args[1]['since'])) {
            $since = Carbon::createFromTimestamp(strtotime($args[1]['since']))
                        ->format('Y-m-d\TH:i:s');
        } else {
            return $since = Carbon::now('UTC')
                     ->subDays(1)
                     ->format('Y-m-d\TH:i:s');
        }
    }

    /**
     * Get datetime value stored in file
     *
     * @param [type] $fileName [description]
     *
     * @return [type] [description]
     */
    public function getDateTime($fileName)
    {
        $path = $this->getFilePath($fileName);

        if (!file_exists($path)) {
            $this->setDateTime($fileName, Carbon::createFromTimestamp(strtotime('50 years ago'))->format('Y-m-d\TH:i:s'));
        }

        return trim(file_get_contents($path));
    }

    /**
     * Sets the date to the config file.
     *
     * @param [type] $fileName [description]
     * @param [type] $date     [description]
     */
    public function setDateTime($fileName, $date)
    {
        $path = $this->getFilePath($fileName);
        file_put_contents($path, $date);
    }

    /**
     * Get absolute file path of the config file.
     *
     * @param [type] $fileName [description]
     *
     * @return [type] [description]
     */
    private function getFilePath($fileName)
    {
        return __DIR__ . '/../config/' . $fileName;
    }

    /**
     * Debug output
     *
     * @param [type]  $o    [description]
     * @param boolean $pass [description]
     *
     * @return [type] [description]
     */
        public function dd($o, $pass = false)
        {
            var_dump($o);

            if (!$pass) {
                exit;
            }
        }
}
