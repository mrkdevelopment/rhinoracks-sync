<?php
/**
 * RhinoRacks Synchronization By Date Tool
 *
 * This plugin synchronizes Rhino Racks products by last update date to your local WP + Woocommerce installation
 *
 * @link              http://mrkdevelopment.com/
 * @copyright  		  M R K Development Pty Ltd.
 * @since             1.0.0
 * @package           Rhino Rack Sync By Date
 * @license 		  http://www.gnu.org/licenses/gpl-2.0.txt
 *
 */

require_once __DIR__.'/API/RhinoHelper.php';

require_once __DIR__.'/API/RhinoRack.php';
require_once __DIR__.'/API/Accessories.php';

require_once __DIR__.'/API/RhinoWCProduct.php';

/**
 * Rhino accessories sync by date class for CLI
 */
class RhinoAccessoriesUpdateByDateCli extends WP_CLI_Command
{

    private $accessories;

    private $accessoryId;

        /**
     * Helper instance
     *
     * @var RhinoHelper
     */
    private $helper;

    /**
     * Command line arguments.
     *
     * @var [type]
     */
    private $args;

    /**
     * Config file which stores date time for --all sync
     *
     * @var string
     */
    private $configFile = 'accessories-sync-datetime.txt';

    /**
     * Runs the syncronization of the products by last update date
     * between RhinoRacks and WP
     */
    public function run()
    {
        $this->args   = func_get_args();
        $this->helper = new RhinoHelper;

        $this->accesoryCategory = $this->helper->addTerm('All Accessories');

        $this->accessories = new Accessories($this->helper->getOptions());

        // Get updated roof racks
         $date = $this->helper->computeSinceArgument($this->configFile, $this->args);
        WP_CLI::line(sprintf("Fetching via GetAccessoriesByDate for accessories updated since %s UTC", $date));

        $params = array('dateString' => $date, 'status' => '1', 'pageIndex' => 1, 'pageSize' => 5);

        while ($this->accessories->GetAccessoriesByDate($params)) {
            // WP_CLI::line(json_encode($params));
            $accessories = $this->accessories->result();

            if (!$accessories->Accessory) {
                break;
            }

            // Save Product
            $i = 0;
            foreach ($accessories->Accessory as $accessory) {
                WP_CLI::line('==================================');
                WP_CLI::line('ID ' . (string) $accessory->ID);
                $product    = new RhinoWCProduct($accessory);
                $productID  = $product->save();
                $categories = $product->getCategories();
                foreach ($categories as $category) {
                    $mainID = $this->helper->addTerm($category['main'], $this->accesoryCategory, $category['main'] . '-' . 'accessories');

                    $titleID = $this->helper->addTerm($category['title'], $mainID, $category['main'] .'-'. $category['title'] . '-' . 'accessories');

                    $response = wp_set_object_terms($productID, array($this->accesoryCategory, $mainID, $titleID), 'product_cat', false);
                    // $this->helper->dd($response, true);
                }

                $i++;
                unset($product);

                if (isset($this->args[1]['all'])) {
                    $this->helper->setDateTime($this->configFile, (string) $accessory->LastUpdated);

                    WP_CLI::line('Last Update Date ' . (string) $accessory->LastUpdated);
                }
            }

            unset($accessories);
            $params['pageIndex']  = $params['pageIndex'] + 1;
        }
    }
}
