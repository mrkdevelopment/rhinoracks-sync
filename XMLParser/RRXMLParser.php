<?php
/**
 * RhinoRacks Synchronization Tool
 *
 * This plugin synchronizes Rhino Racks products to your local WP + Woocommerce installation
 *
 * @link              http://mrkdevelopment.com/
 * @copyright  		  M R K Development Pty Ltd.
 * @since             1.0.0
 * @package           Rhino Rack Sync
 * @license 		  http://www.gnu.org/licenses/gpl-2.0.txt
 *
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
global $woocommerce;

require_once __DIR__ . '/../API/RhinoWCProduct.php';

/**
 * RRParser
 * Parsing a callback from RhinoRacks
 */
class RRXMLParser
{

    private $xml;

    public function __construct($xml)
    {
        $this->products = $xml;
    }

    public function parse()
    {
        $products = $this->products;
        // var_dump($products);

        $numItems = $products->params->param[0]->value->int;
        $language = $products->params->param[2]->value->struct->member[0]->value->int;
        $currency = $products->params->param[2]->value->struct->member[1]->value->int;

        $productList = array();
        foreach ($products->params->param[1]->value->struct->member as $productInfo) {
            $nameValue = $this->getNameVal($productInfo);

            $product = array(
                'productId'           => $nameValue['stock_code'],
                'productName'         => $nameValue['name'],
                'productPrice'        => $nameValue['price'],
                'productTax'          => $nameValue['tax_inclusive'],
                'productImg'          => $nameValue['image_url'],
                'productQuantity'     => $nameValue['quantity'],
                'productAvailability' => $nameValue['availability'],
            );

            // var_dump($product);

            $productList[] = $product;
        }

        $this->generateProducts($productList);
    }

    /**
     * Get the data in name value for the given xml struct
     *
     * @param [type] $row [description]
     *
     * @return [type] [description]
     */
    private function getNameVal($row)
    {
        $response = array();
        foreach ($row->value->struct->member as $member) {
            // var_dump($member);

            if (is_string($member->value)) {
                $value = $member->value;
            }

            if (isset($member->value->string)) {
                $value = (string) $member->value->string;
            }

            if (isset($member->value->double)) {
                $value = (double) $member->value->double;
            }

            if (isset($member->value->int)) {
                $value = (int) $member->value->int;
            }

            $response[(string)$member->name] = $value;
        }

        return $response;
    }

    /**
     * For each product, will test if it exists in database and update or insert it
     * @param  Array[] $products The list of products
     * @return void
     */
    private function generateProducts($products)
    {
        $this->productIds = array();

        foreach ($products as $product) {
            if ($id = $this->productExists($product)) {
                $this->updateProduct($product, $id);
            } else {
                $id = $this->insertProduct($product);
            }

            $quantity = (int) $product['productQuantity'];

            for ($i = 0; $i < $quantity; $i++) {
                $this->productIds[] = $id;
            }
        }

        $this->addProductsToCart($products);
    }

    /**
     * Inser product in the database
     * @param array $product the product to inster
     */
    private function insertProduct($product)
    {
        $post = array(
             'post_author'  => 1,
             'post_content' => '',
             'post_status'  => "publish",
             'post_title'   => $product['productName'],
             'post_parent'  => '',
             'post_type'    => "product",
         );

          //Create post
         $post_id = wp_insert_post($post);

        if ($post_id) {
            $this->updateProduct($product, $post_id);
        }

        // download image
        $wp_upload_dir = wp_upload_dir();
        $url           = $product['productImg'];
        $urlPathinfo   = pathinfo($url);

        $localPath = $wp_upload_dir['basedir'] . '/rhinoracks/' . $urlPathinfo['basename'];

        $p         = new RhinoWCProduct(array());
        $p->downloadImage($url, $localPath);
        $p->associateAttachment($localPath, $post_id);

        return $post_id;
    }

    /**
     * checks if the produc exists
     */
    public function productExists($product)
    {
        $productSKU = $product['productId'];
        $args       = array(
            'posts_per_page' => -1,
            'post_type'      => 'product',
            'meta_query'     => array(array('key' => '_sku', 'value' => $productSKU)),
        );
        $the_query = new WP_Query( $args );
        while ( $the_query->have_posts() ) {
            $the_query->the_post();
            $id = get_the_ID();

            return $id;
        }
    }

    /**
     * Updates a given product
     */
    public function updateProduct($product, $post_id)
    {
        $productId    = $product['productId'];
        $productPrice = $product['productPrice'];
        update_post_meta( $post_id, '_sku', $productId);
        update_post_meta( $post_id, '_price', $productPrice);
    }

    /**
     * Fill the car with the selected products
     * @param array[] $products list of all the products
     */
    public function addProductsToCart($products)
    {
        global $woocommerce;

        foreach ($this->productIds as $id) {
            $woocommerce->cart->add_to_cart($id);
        }
    }
}
