<?php
/**
 * RhinoRacks Synchronization Tool
 *
 * This plugin synchronizes Rhino Racks products to your local WP + Woocommerce installation
 *
 * @link              http://mrkdevelopment.com/
 * @copyright  		  M R K Development Pty Ltd.
 * @since             1.0.0
 * @package           Rhino Rack Sync
 * @license 		  http://www.gnu.org/licenses/gpl-2.0.txt
 *
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
require_once('RRXMLParser.php');

/**
 * RRParser
 * Parsing a callback from RhinoRacks
 */
class RRParser
{

    private $url = '';

    public function __construct($url)
    {
        $this->url = $url;

        $requestURI    = strtok($_SERVER['REQUEST_URI'], '?');
        $fileInputData = file_get_contents('php://input');
        if ($xml = simplexml_load_string($fileInputData)) {
            // generate token
            $token = md5(rand(00000, 99999));

            // save in DB the temporary data
            $post = array(
              'post_title'   => $token,
              'post_status'  => 'publish',
              'post_content' => htmlspecialchars($fileInputData),
              'post_type'    => 'rhino-cart',
            );
            wp_insert_post($post);
            echo sprintf("
            <script>
                window.location.href = '%s?token=%s';
            </script>", $this->url, $token);
        }

        if ($requestURI == '/cart/'  && isset($_GET['token'])) {
            $token = $_GET['token'];

            $posts = get_posts(array(
                'posts_per_page' => 1,
                'post_type'      => 'rhino-cart',
                'post_title'     => $token,
              ));

            if (sizeOf($posts) == 1) {
                $post    = $posts[0];
                $content = htmlspecialchars_decode($post->post_content);
                $xml     = simplexml_load_string($content);
                $parser  = new RRXMLParser($xml);
                $parser->parse();

                wp_delete_post($post->ID, true);
            }

            echo sprintf("
            <script>
                window.location.href = '%s';
            </script>", $this->url);
        }
    }
}
