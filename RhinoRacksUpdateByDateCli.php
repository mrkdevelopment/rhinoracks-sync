<?php
/**
 * RhinoRacks Synchronization By Date Tool
 *
 * This plugin synchronizes Rhino Racks products by last update date to your local WP + Woocommerce installation
 *
 * @link              http://mrkdevelopment.com/
 * @copyright  		  M R K Development Pty Ltd.
 * @since             1.0.0
 * @package           Rhino Rack Sync By Date
 * @license 		  http://www.gnu.org/licenses/gpl-2.0.txt
 *
 */

require_once __DIR__.'/API/RhinoHelper.php';
require_once __DIR__.'/API/RhinoRack.php';
require_once __DIR__.'/API/Accessories.php';

require_once __DIR__.'/API/RhinoWCProduct.php';
require_once __DIR__.'/API/RoofRacks.php';
require_once __DIR__.'/API/Vehicles.php';

/**
 * Rhino racks sync by date class for CLI
 */
class RhinoRacksUpdateByDateCli extends WP_CLI_Command
{

    private $roofRacks;

    /**
     * Helper instance
     *
     * @var RhinoHelper
     */
    private $helper;

    /**
     * Command line arguments.
     *
     * @var [type]
     */
    private $args;

    /**
     * Config file which stores date time for --all sync
     *
     * @var string
     */
    private $configFile = 'racks-sync-datetime.txt';

    /**
     * Runs the synchronization of the products by last update date
     * between RhinoRacks and WP
     */
    public function run()
    {
        $this->args   = func_get_args();
        $this->helper = new RhinoHelper;

        $this->roofRacks = new RoofRacks($this->helper->getOptions());

        // Get updated roof racks
        $date           = $this->helper->computeSinceArgument($this->configFile, $this->args);

        WP_CLI::line(sprintf("Calling GetRoofRacksForDate for racks since %s UTC", $date));

        $params = array('dateFromString' => $date, 'status' => '1', 'pageIndex' => 1, 'pageSize' => 5);

        while ($this->roofRacks->GetRoofRacksForDate($params)) {
            // WP_CLI::line("Params - " . json_encode($params));
            $updatedRacks = $this->roofRacks->result();

            if (!$updatedRacks->RoofRack) {
                break;
            }

            // Save products
            $i = 0;
            foreach ($updatedRacks->RoofRack as $roofRack) {
                WP_CLI::line("=================================");
                WP_CLI::line("Internal ID " . (string) $roofRack->ID);
                WP_CLI::line("SKU " . (string) $roofRack->StockCode);

                $product    = new RhinoWCProduct($roofRack);
                $postID     = $product->save();

                $product->syncRackCategories($postID);

                $i++;
                unset($product);

                if (isset($this->args[1]['all'])) {
                    $this->helper->setDateTime($this->configFile, (string) $roofRack->LastUpdated);

                    WP_CLI::line('Last Update Date ' . (string) $roofRack->LastUpdated);
                }
            }
            unset($updatedRacks);
            $params['pageIndex'] = $params['pageIndex'] + 1;
        }
    }
}
